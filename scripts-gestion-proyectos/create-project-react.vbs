'============== Proceso Principal =============='
Sub Main()
	'******************************Cargando Librerias******************************'
	' Creando objeto Shell para ejecutar comandos
	Set wScriptShell = CreateObject("WScript.Shell")
	' Creando objeto FileSystemObject para manipular directorios
	Set scriptFileSystem = CreateObject("Scripting.FileSystemObject")

	'*******************************Definiendo rutas*******************************'
	'Creando variables de rutas
	Dim currentDirectory, nodePortableFolder, projectsFolder
	' Obtener la ruta del directorio actual
	currentDirectory = scriptFileSystem.GetParentFolderName(scriptFileSystem.GetAbsolutePathName("."))
	' Ruta de la subcarpeta "node-portable"
	nodePortableFolder = currentDirectory & "\core\node-portable"
	' Ruta de la subcarpeta "projects"
	projectsFolder = currentDirectory & "\projects"

	'*****************************Invocando funciones******************************'
	'Verificamos que existe una version del Node cargada en la subcarpeta \core\node-portable 
	isValidVersion = checkInstalledVersionNode(nodePortableFolder, scriptFileSystem)

	If isValidVersion Then
		' Cargando un cuadro de texto para ingresar el nombre del nuevo proyecto
		nameNewProject = getNameNewProjectReact()

		If nameNewProject <> "" Then
			' Cargamos unas variables de entorno temporales para ejecutar los comandos del node
			loadTemporaryEnvironmentVariables wScriptShell, nodePortableFolder
			' Ejecutamos el comando de creacion de proyectos con el nombre ingresado anteriormente
			createProjectReact nameNewProject, projectsFolder, wScriptShell
		End If
	End If
End Sub

'===============Funciones===============
Function checkInstalledVersionNode(param_nodePath, param_script)
	Dim isValidVersion, pathNodeModules
	isValidVersion = False
	pathNodeModules = ""

	If param_script.FolderExists(param_nodePath) Then
		pathNodeModules = param_nodePath & "\node_modules"
		If param_script.FolderExists(pathNodeModules) Then
			isValidVersion = True
		Else
			MsgBox "No hay una version del Node cargada", vbCritical
		End If
	Else
		MsgBox "No existe la carpeta node_portable", vbCritical
	End If

	checkInstalledVersionNode = isValidVersion
End Function

Function getNameNewProjectReact()
	Dim nameProject
	nameProject = ""

	nameProject = InputBox("Ingrese el nombre para su nuevo proyecto", "Generador proyectos - React")
	nameProject = Trim(nameProject)

	If nameProject = "" Then
		MsgBox "No se ingreso ningun nombre para el proyecto.", vbExclamation
	End If 
		
	getNameNewProjectReact = nameProject
End Function

Function loadTemporaryEnvironmentVariables(param_shell, param_NodeFolderPath)
	param_shell.Environment("Process")("PATH") = param_NodeFolderPath & ";" & param_shell.Environment("Process")("PATH")
	param_shell.Environment("Process")("NODE_PATH") = param_NodeFolderPath
End Function

Function createProjectReact(param_nameProject, param_currentFolderPath, param_shell)
	Dim command_cmd_project, command_create_project

	command_cmd_project = "cmd /k cd """ & param_currentFolderPath & """"
	command_create_project = "npm create vite@latest """ & param_nameProject & """ -- --template react{ENTER}"
		
	param_shell.Run command_cmd_project, 1, False
	WScript.Sleep 2000
	param_shell.SendKeys command_create_project
End Function

'==============Ejecuciones==============
Main()