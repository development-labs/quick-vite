'============== Proceso Principal =============='
Sub Main()
	'******************************Cargando Librerias******************************'
	' Creando objeto Shell para ejecutar comandos
	Set wScriptShell = CreateObject("WScript.Shell")
	' Creando objeto FileSystemObject para manipular directorios
	Set scriptFileSystem = CreateObject("Scripting.FileSystemObject")
	' Creando objeto Application para abrir interfaces
	Set shellApplication = CreateObject("Shell.Application")

	'*******************************Definiendo rutas*******************************'
	'Creando variables de rutas
	Dim currentDirectory, nodePortableFolder, projectsFolder
	' Obtener la ruta del directorio actual
	currentDirectory = scriptFileSystem.GetParentFolderName(scriptFileSystem.GetAbsolutePathName("."))
	' Ruta de la subcarpeta "node-portable"
	nodePortableFolder = currentDirectory & "\core\node-portable"
	' Ruta de la subcarpeta "projects"
	projectsFolder = currentDirectory & "\projects"

	'*****************************Invocando funciones******************************'
	'Verificamos que existe una version del Node cargada en la subcarpeta \core\node-portable 
	isValidVersion = checkInstalledVersionNode(nodePortableFolder, scriptFileSystem)

	If isValidVersion Then
		' Verificamos que existan proyectos dentro de la carpeta "projects"
		Set folder = scriptFileSystem.GetFolder(projectsFolder)
		
		If folder.SubFolders.Count > 0 Then
			' Se carga un explorador de archivos para seleccionar una ruta de un proyecto del node portable
			projectPathSelected = loadLocalProjectPath(projectsFolder, shellApplication)
			If projectPathSelected <> "" Then
				' Cargamos unas variables de entorno temporales para ejecutar los comandos del node
				loadTemporaryEnvironmentVariables wScriptShell, nodePortableFolder
				' Ejecutamos el comando de instalacion de librerias dependientes para el proyecto
				installProjectPackages projectPathSelected, wScriptShell, scriptFileSystem
			End If
		Else
			MsgBox "La carpeta de proyectos esta vacia.", vbExclamation
		End If
	End If
End Sub

'===============Funciones===============
Function checkInstalledVersionNode(param_nodePath, param_script)
	Dim isValidVersion, pathNodeModules
	isValidVersion = False
	pathNodeModules = ""

	If param_script.FolderExists(param_nodePath) Then
		pathNodeModules = param_nodePath & "\node_modules"
		If param_script.FolderExists(pathNodeModules) Then
			isValidVersion = True
		Else
			MsgBox "No hay una version del Node cargada", vbCritical
		End If
	Else
		MsgBox "No existe la carpeta node_portable", vbCritical
	End If

	checkInstalledVersionNode = isValidVersion
End Function

Function loadLocalProjectPath(param_projectsPath, param_script)
    Dim selectedPath
    selectedPath = ""
	
	Set objFolder = param_script.BrowseForFolder(0, "Seleccione el proyecto que desee instalar sus paquetes", 0, param_projectsPath)
	If Not objFolder Is Nothing Then
		If objFolder.Self.Path <> param_projectsPath Then
			selectedPath = objFolder.Self.Path
		Else
			MsgBox "Seleccione un proyecto de la lista.", vbExclamation
		End If
	Else
		MsgBox "No se selecciono ningun proyecto.", vbExclamation
	End If
    
    loadLocalProjectPath = selectedPath
End Function

Function loadTemporaryEnvironmentVariables(param_shell, param_NodeFolderPath)
	param_shell.Environment("Process")("PATH") = param_NodeFolderPath & ";" & param_shell.Environment("Process")("PATH")
	param_shell.Environment("Process")("NODE_PATH") = param_NodeFolderPath
End Function

Function installProjectPackages(param_projectPath, param_shell, param_scriptFile)
	Dim command_cmd_project, command_install_packages, fileName, filePath
	fileName = "package.json"

	filePath = param_scriptFile.BuildPath(param_projectPath, fileName)

	If param_scriptFile.FileExists(filePath) Then
		command_cmd_project = "cmd /k cd """ & param_projectPath & """"
		command_install_packages = "npm install{ENTER}"

		param_shell.Run command_cmd_project, 1, False
		WScript.Sleep 2000
		param_shell.SendKeys command_install_packages
		WScript.Sleep 500
		param_shell.SendKeys "{ENTER}"
	Else
		MsgBox "Este proyecto no cuenta con el archivo package.json.", vbCritical
	End If
End Function

'==============Ejecuciones==============
Main()