## QuickVite

### 1. Introducción:
- ¡Bienvenidos a la primera versión de QuickVite! Esta pequeña pero poderosa herramienta está diseñada para simplificar la gestión de proyectos basados en React. En el vasto mundo del desarrollo web, encontramos numerosas herramientas que optimizan procesos y configuraciones en proyectos. Sin embargo, a menudo nos enfrentamos al desafío de configurar nuestro entorno de desarrollo, lidiar con conflictos de puertos y gestionar versiones obsoletas de herramientas.

QuickVite viene al rescate para eliminar estas barreras. Su objetivo es permitirte centrarte en el desarrollo sin preocuparte por las configuraciones del sistema o las versiones anticuadas de las herramientas. Con QuickVite, puedes crear, instalar y cambiar entre versiones de proyectos con facilidad, todo sin necesidad de abrir configuraciones del sistema.

### 2. Detalles:
- **Compatibilidad:** Sistemas operativos Windows 7, 8, 8.1, 10, 11
- **Version:** 1.0
- **Autor:** Steven Pacheco Rios - @steven.pacheco9822
- **Gitlab:** [QuickVite en GitLab](https://gitlab.com/development-labs/quick-vite)
- **Fecha de lanzamiento:** 17/04/2024
	
### 3. Tecnologías:
#### 3.1. Node:
Es un entorno de ejecución de JavaScript que permite ejecutar código JavaScript en el lado del servidor. Es conocido por su eficiencia y escalabilidad, lo que lo hace ideal para construir aplicaciones web rápidas y escalables.
	
#### 3.2. React:
Es una biblioteca de JavaScript utilizada para construir interfaces de usuario interactivas. Desarrollada por Facebook, React utiliza un enfoque basado en componentes que facilita la creación de aplicaciones web modulares y reutilizables.
	
#### 3.3. Vite:
Es una herramienta de construcción de proyectos desarrollada para aplicaciones web modernas. Con un enfoque en la velocidad, Vite proporciona un servidor de desarrollo rápido y una experiencia de desarrollo optimizada al utilizar tecnologías como JavaScript moderno, CSS y HTML.
	
### 4. Estructura:
Agruparemos a los archivos de la siguiente manera:

#### 4.1. Gestión de proyectos:
- **create-project-react(vbs):**
  - **Función:** Ayuda a crear proyectos React basados en la herramienta Vite, alojándolos posteriormente en la carpeta "projects".
  - **Uso:** Al ejecutar este archivo, se abrirá una ventana donde podrás ingresar el nombre del proyecto. Luego, se abrirá una consola del sistema de Windows donde se ejecutará un comando para crear el proyecto. Espera unos segundos a que se imprima el comando en la consola y luego puedes minimizarla, pero no la cierres hasta que veas el mensaje "Done. Now run". Si hay algún error o demora, cierra la consola y vuelve a ejecutar el archivo.

- **install-packages-project(vbs):**
  - **Función:** Ayuda a instalar los paquetes necesarios del proyecto para su correcto funcionamiento.
  - **Uso:** Al ejecutar este archivo, se mostrará un explorador de archivos con una lista de los proyectos alojados en la carpeta "projects". Selecciona uno y pulsa aceptar; luego se abrirá una consola del sistema de Windows donde se ejecutará un comando para instalar los paquetes necesarios. Espera unos segundos a que se imprima el comando en la consola y luego puedes minimizarla, pero no la cierres hasta que veas una serie de mensajes como "packages" y "vulnerabilities". Si hay algún error o demora, cierra la consola y vuelve a ejecutar el archivo.

- **run-project-react(vbs):**
  - **Función:** Ayuda a ejecutar proyectos que tengan instalados sus paquetes.
  - **Uso:** Al ejecutar este archivo, se mostrará un explorador de archivos con una lista de los proyectos alojados en la carpeta "projects". Selecciona uno y pulsa aceptar; luego se abrirá una consola del sistema de Windows donde se ejecutará un comando para ejecutar el proyecto seleccionado. Espera unos segundos a que se imprima el comando en la consola y luego ya tendras en ejecución tu proyecto, puedes minimizarla si gustas. Si necesitas detener la ejecución, simplemente cierra la consola o presiona la combinación de teclas "CTRL" + "C" dos veces.

#### 4.2. Consolas configuradas:
- **cmd-node(vbs):**
  - **Función:** Abre una consola configurada para ejecutar comandos de Node.
  - **Uso:** Si tienes conocimientos previos sobre comandos de Node, puedes ejecutar este archivo para acceder a una consola configurada y lista para ser utilizada.

- **cmd-node-vscode(bat):**
  - **Función:** Configura la terminal del Visual Studio Code para ejecutar comandos de Node.
  - **Uso:** Abre la carpeta principal en VS Code, busca la opción "Terminal" en la barra de menú, posteriormente se abrirá una pestaña de la "terminal" y buscaremos entre sus opciones el boton "+" y desplegaremos las demas opciones para seleccionar finalmente aquella que diga "Command Prompt". Una vez abierto una nueva terminal de este tipo, ingresaremos a la carpeta "scripts-consolas-configuradas" mediante el comando "cd ruta", luego escribiremos el nombre del archivo "cmd-node-vscode" pulsando luego la tecla "TAB" ubicada aproximadamente al lado izquierdo de la tecla "Q"; una vez pulsado veremos un autocompletado como este ".\cmd-node-vscode.bat" para pulsar la tecla enter como paso final.

#### 4.3. Gestión de Node:
- **load-node-version(vbs):**
  - **Función:** Permite cambiar la versión de Node.
  - **Uso:** Al ejecutar este archivo se abrirá un explorador de archivos con una lista de las versiones alojadas en la carpeta "\core\versiones". Selecciona la versión deseada y espera a que se complete el proceso. Una vez finalizado, verás un mensaje confirmando la carga de la nueva versión de Node. Si deseas descargar otra version puedes abrir el archivo "Página de descarga Node" el cual te llevara a la página de descargas de versiondes de Node, seleccionas una en especifico guiandote siempre de la fecha de lanzamiento y luego descargas la opción que tenga la extensión ".7z o .zip" para descomprimirla finalmente en la carpeta "\core\versiones"

- **version-node(vbs):**
  - **Función:** Informa la versión actual de Node.
  - **Uso:** Al ejecutar este archivo se mostrará un mensaje que indica la versión actual del Node en esta herramienta portatil.

Con QuickVite, simplifica tu flujo de trabajo y dedica más tiempo al desarrollo de tus proyectos sin preocuparte por las configuraciones y versiones. ¡Disfruta de la eficiencia y productividad que ofrece QuickVite!
