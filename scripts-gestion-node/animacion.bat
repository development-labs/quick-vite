@echo off
setlocal EnableDelayedExpansion

rem Define la longitud de la barra de carga
set "bar_length=20"

:main_loop
rem Limpia la pantalla
cls

rem Muestra el mensaje de carga
echo Por favor, espera...

rem Cambia el título de la ventana del CMD
title Proceso cambiar version Node

rem Inicializa la variable de progreso
set "progress=0"

rem Loop para simular el llenado de la barra de carga
:loading_loop
rem Muestra la barra de carga actual
set /a "filled_length=(progress * bar_length) / 40"
set "bar="
for /l %%i in (1,1,%filled_length%) do (
    set "bar=!bar!#"
)
set /p "= [!bar!]" <nul
rem Incrementa el progreso
set /a "progress+=5"
rem Espera un momento antes de continuar (para simular el proceso)
timeout /t 0 >nul
rem Borra la barra de carga actual
set /p "=" <nul
rem Si el progreso es menor o igual a 100, continúa el proceso de carga
if %progress% leq 100 (
    goto :loading_loop
)

rem El proceso ha finalizado, muestra un mensaje de finalización y espera antes de reiniciar
echo.
echo En proceso...
timeout /t 2 >nul
goto :main_loop
