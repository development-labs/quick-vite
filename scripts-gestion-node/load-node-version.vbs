'============== Proceso Principal =============='
Sub Main()
	'******************************Cargando Librerias******************************'
	' Creando objeto Shell para ejecutar comandos
    Set wScriptShell = CreateObject("WScript.Shell")
	' Creando objeto FileSystemObject para manipular directorios
	Set scriptFileSystem = CreateObject("Scripting.FileSystemObject")
	' Creando objeto Application para abrir interfaces
	Set shellApplication = CreateObject("Shell.Application")

	'*******************************Definiendo rutas*******************************'
	'Creando variables de rutas
	Dim currentDirectory, nodePortableFolder, versionsNode
	' Obtener la ruta del directorio actual
	currentDirectory = scriptFileSystem.GetParentFolderName(scriptFileSystem.GetAbsolutePathName("."))
	' Ruta de la subcarpeta "node-portable"
	nodePortableFolder = currentDirectory & "\core\node-portable"
	' Ruta de la subcarpeta "node-versiones"
	versionsNode = currentDirectory & "\core\versions"
	
	'*****************************Invocando funciones******************************'
    'Verificamos que existe una version del Node cargada en la subcarpeta \core\node-portable 
	isValidVersion = checkInstalledVersionNode(nodePortableFolder, scriptFileSystem)

	If isValidVersion Then
    	' Verificamos que existan versiones descargadas dentro de la carpeta "\core\versions"
		Set folder = scriptFileSystem.GetFolder(versionsNode)

		If folder.SubFolders.Count > 0 Then
            ' Se carga un explorador de archivos para seleccionar la nueva version del node portable
            newVersionNode = loadNewNodeJSVersionPath(versionsNode, shellApplication, scriptFileSystem)
            ' Verificamos que el valor cargado sea valido
            If newVersionNode <> "" Then
                ' Lanzamos la animacion de carga
                wScriptShell.Run "animacion.bat", 1, False
                ' Eliminamos la vieja version del node
                If removeOldNodeJSCoreFolder(nodePortableFolder, scriptFileSystem) Then
                    ' Copiamos la ruta de la nueva version cargada previamente
                    createNodeJSCoreFolder newVersionNode, nodePortableFolder, scriptFileSystem, wScriptShell
                End If
             End If
        Else
			MsgBox "La carpeta de versiones esta vacia.", vbExclamation
        End If
    End If
End Sub

'===============Funciones===============
Function checkInstalledVersionNode(param_nodePath, param_script)
	Dim isValidVersion, pathNodeModules
	isValidVersion = False
	pathNodeModules = ""

	If param_script.FolderExists(param_nodePath) Then
		pathNodeModules = param_nodePath & "\node_modules"
		isValidVersion = True
	Else
		MsgBox "No existe la carpeta node_portable", vbCritical
	End If

	checkInstalledVersionNode = isValidVersion
End Function

Function loadNewNodeJSVersionPath(param_versionsNodePath, param_script, param_scriptFile)
    Dim selectedPath, fileName, filePath, folderName, folderPath
    selectedPath = ""
	fileName = "node.exe"
    folderName = "node_modules"
    
    Set objFolder = param_script.BrowseForFolder(0, "Seleccione la version de Node a cargar", 0, param_versionsNodePath)
    If Not objFolder Is Nothing Then
		If objFolder.Self.Path <> param_versionsNodePath Then
        	filePath = param_scriptFile.BuildPath(objFolder.Self.Path, fileName)
            folderPath = param_scriptFile.BuildPath(objFolder.Self.Path, folderName)
            If param_scriptFile.FolderExists(folderPath) And param_scriptFile.FileExists(filePath) Then
                selectedPath = objFolder.Self.Path
            Else
                MsgBox "La version seleccionada no es valida", vbCritical 
            End If
        Else
			MsgBox "Seleccione una version de la lista.", vbExclamation
		End If
    Else
        MsgBox "No se selecciono ninguna version.", vbExclamation 
    End If
    
    loadNewNodeJSVersionPath = selectedPath
End Function

Function removeOldNodeJSCoreFolder(param_nodeOldVersion, param_script)
    Dim success
    success = False

    If param_script.FolderExists(param_nodeOldVersion) Then
        param_script.DeleteFolder param_nodeOldVersion, True
        If Not param_script.FolderExists(param_nodeOldVersion) Then
            success = True
        Else
            MsgBox "Error al eliminar la carpeta " & """" & param_nodeOldVersion & """", vbCritical
        End If
    Else
        success = True
    End If
    
    removeOldNodeJSCoreFolder = success
End Function

Function createNodeJSCoreFolder(param_sourceFolder, param_destinationFolder, param_script, param_scriptShell)
    Dim success, command_version_node
    success = False
    command_version_node = "wscript.exe version-node.vbs"
    
    If Not param_script.FolderExists(param_destinationFolder) Then
        param_script.CreateFolder param_destinationFolder
    End If

    param_script.CopyFolder param_sourceFolder, param_destinationFolder
    If param_script.FolderExists(param_destinationFolder) Then
        success = True
    End If
    
    If success Then
		animationStopLoading param_scriptShell
        param_scriptShell.Run command_version_node, 1, True
    Else
        MsgBox "Error al copiar la carpeta.", vbCritical
    End If
End Function

Function animationStopLoading(param_scriptShell)
    ' Obtener una instancia de WMI
    Dim objWMIService, colProcesses, objProcess, command, nameScript
    nameScript = "animacion.bat"

    ' Establecer la consulta WMI para obtener los procesos cmd.exe
    Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
    Set colProcesses = objWMIService.ExecQuery("SELECT * FROM Win32_Process WHERE Name='cmd.exe'")

    ' Recorrer los procesos cmd.exe
    For Each objProcess in colProcesses
        ' Verificar si el proceso cmd.exe está ejecutando el archivo .bat
        command = objProcess.CommandLine
        If InStr(command, nameScript) > 0 Then
            ' Terminar el proceso
            objProcess.Terminate()
            Exit For
        End If
    Next
End Function

'==============Ejecuciones==============
Main()