'============== Proceso Principal =============='
Sub Main()
	'******************************Cargando Librerias******************************'
	' Creando objeto Shell para ejecutar comandos
	Set wScriptShell = CreateObject("WScript.Shell")
	' Creando objeto FileSystemObject para manipular directorios
	Set scriptFileSystem = CreateObject("Scripting.FileSystemObject")
	
	'*******************************Definiendo rutas*******************************'
	'Creando variables de rutas
	Dim currentDirectory, nodePortableFolder, projectsFolder
	' Obtener la ruta del directorio actual
	currentDirectory = scriptFileSystem.GetParentFolderName(scriptFileSystem.GetAbsolutePathName("."))
	' Ruta de la subcarpeta "node-portable"
	nodePortableFolder = currentDirectory & "\core\node-portable"

	'*****************************Invocando funciones******************************'
	'Verificamos que existe una version del Node cargada en la subcarpeta \core\node-portable 
	isValidVersion = checkInstalledVersionNode(nodePortableFolder, scriptFileSystem)

	If isValidVersion Then
		' Cargamos unas variables de entorno temporales para ejecutar los comandos del node
		loadTemporaryEnvironmentVariables wScriptShell, nodePortableFolder
		' Se abre la consola configurada con el entorno temporal del Node
		checkCurrentVersionNode wScriptShell
	End If
End Sub

'===============Funciones===============
Function checkInstalledVersionNode(param_nodePath, param_script)
	Dim isValidVersion, pathNodeModules
	isValidVersion = False
	pathNodeModules = ""

	If param_script.FolderExists(param_nodePath) Then
		pathNodeModules = param_nodePath & "\node_modules"
		If param_script.FolderExists(pathNodeModules) Then
			isValidVersion = True
		Else
			MsgBox "No hay una version del Node cargada", vbCritical
		End If
	Else
		MsgBox "No existe la carpeta node_portable", vbCritical
	End If

	checkInstalledVersionNode = isValidVersion
End Function

Function loadTemporaryEnvironmentVariables(param_shell, param_NodeFolderPath)
	' Definimos las variables de entorno temporales para ejecutar los comandos del node
	param_shell.Environment("Process")("PATH") = param_NodeFolderPath & ";" & param_shell.Environment("Process")("PATH")
	param_shell.Environment("Process")("NODE_PATH") = param_NodeFolderPath
End Function

Function checkCurrentVersionNode(param_shell)
    Dim command_npm_version, strOutput

    command_npm_version = "node --version"
    Set objExec = param_shell.Exec("%comspec% /c """ & command_npm_version & """")
    strOutput = objExec.StdOut.ReadAll()

    MsgBox "Version de node: " & strOutput, vbInformation
End Function

'==============Ejecuciones==============
Main()