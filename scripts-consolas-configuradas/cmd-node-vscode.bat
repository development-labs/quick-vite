@echo off
rem Obtener la ruta del directorio actual del script
set "currentDir=%~dp0"

rem Obtener la ruta del directorio padre
for %%I in ("%currentDir%..") do set "parentDir=%%~fI"

rem Definir las rutas de las variables de entorno
set "nodePortableFolder=%parentDir%\core\node-portable"

rem Verificar si la carpeta de Node.js existe
if not exist "%nodePortableFolder%" (
    echo Error: No se encontro la carpeta de "node-portable" en la ruta especificada.
    exit /b 1
)

rem Verificar si la carpeta node_modules existe dentro de la carpeta de Node.js
if not exist "%nodePortableFolder%\node_modules" (
    echo Error: No se encontro la carpeta 'node_modules' en la carpeta "node-portable".
    exit /b 1
)

rem Verificar si el archivo node.exe existe dentro de la carpeta de Node.js
if not exist "%nodePortableFolder%\node.exe" (
    echo Error: No se encontro el archivo 'node.exe' en la carpeta "node-portable".
    exit /b 1
)

rem Actualizar las variables de entorno temporales
set "PATH=%nodePortableFolder%;%PATH%"
set "NODE_PATH=%nodePortableFolder%"
